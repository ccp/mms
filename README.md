# MMS tech tasks
The Algorithm task is done in **find_min.py**
The GPS task is done in **gps_plot.py**

export PYTHONPATH=.


## GPS

Server-side rendering only for now. Making use of the HERE Maps API with a Free API key
https://developer.here.com/

>GPS Data files are descibed in the **data** folder pdf. Data was downloaded from MS (Taxi driver data)
>The application uses only the **data.csv** file in the data folder
