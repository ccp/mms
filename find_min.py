"""
file: find_min.py
brief: Take a sorted array and a number and return the number or next smallest
author: Caswell Pieters
date: 27 February 2022
"""

from bisect import bisect_left


def binary_find_first(input_list, number):
    """
    Do a bisect to find the leftmost number in the list.
    :param input_list : List to search.
    :type input_list : list
    :param number : The number to search for
    :type number : int
    :return : The index of the number to search or next smallest
    :rtype : int
    """

    index = bisect_left(input_list, number)
    # number is present
    if index != len(input_list) and input_list[index] == number:
        return index
    # number not in list, take the next smallest
    elif index:
        return (index-1)
    # index is zero here , neither number or smaller present
    else:
        return -1


def get_list_input():
    """
    Get list input from user
    :return : The user given list, sorted
    :rtype : list
    """

    # create empty list
    new_list = []

    # get number of elements
    try:
        n = int(input("Enter number of elements : "))
    except ValueError:
        print('Please enter a number.')
        exit(1)
    
    if (n > 0):
        # populate
        for i in range(n):
            try:
                element = int(input("Enter a list number:"))
            except ValueError:
                print('You have entered an incorrect value. Please try again')
                exit(1)
            new_list.append(element)
    else:
        print('Please enter a positive number for list size')
        exit(1)

    # sort the list
    new_list.sort()

    print(new_list)
 
    return new_list


def main():
    """
    Accepts the inputs and returns the given number os next smallest
    :return : None
    """

    sorted_list = get_list_input()

    num_input = input('Input the number to search:')

    # We start with a sorted list
    search_number = int(num_input)

    index = binary_find_first(sorted_list, search_number)
    if index == -1:
        print('No viable value')
    else:
        print(f'Desired value is {sorted_list[index]}')


if __name__ == "__main__":
    main()
