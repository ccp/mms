"""
file: test_get_gps.py
brief: Unit test for get gps data
author: Caswell Pieters
date: 2 March 2022
"""

import get_gps

def test_make_seconds():
    """Test for turning date text into seconds"""

    date = "2008-02-02 16:36:08"

    assert get_gps.make_seconds(date) == 1201962968.0; 'test failed'