"""
file: test_find_min.py
brief: Unit test for find value or second smallest algo
author: Caswell Pieters
date: 28 Feburary 2022
"""

import find_min

def test_binary_find_first():
    """Test the binary search to find minium"""

    # list must be sorted at this point
    list_1 = [2, 3, 5, 6, 10]

    search_number_1 = 2
    search_number_2 = 9
    search_number_3 = 1

    # find a number in the list
    assert find_min.binary_find_first(list_1, search_number_1) == 0, 'test failed'
    # find next smallest number
    assert find_min.binary_find_first(list_1, search_number_2) == 3, 'test failed'
    # number not in list
    assert find_min.binary_find_first(list_1, search_number_3) == -1, 'test failed'
