"""
file: get_gps.py
brief: Read in a CSV file with GPS data and output latitudes, longitudes and average speed
author: Caswell Pieters
date: 27 February 2022
"""

from datetime import datetime

from geopy import distance
import pandas as pd

def make_seconds(time_element):
    """
    Returns the date in seconds
    :param time_element : The date/time from the GPS data
    :type time_element : String
    :return : Date time in seconds
    :rtype :int
    """

    date = datetime.strptime(time_element, '%Y-%m-%d %H:%M:%S')
    time_stamp = date.timestamp()
    return time_stamp


def get_data(file_path):
    """
    Returns a pandas dataframe from the file path for gps data
    :param file_path : The file path to the gps file
    :type file_path : string
    :return : The pandas dataframe
    :rtype : DataFram
    """

    # read into a pandas dataframe
    try:
        data = pd.read_csv(file_path, names=['ID', 'TIME', 'LONTITUDE', 'LATITUDE'], sep=',')
    except FileNotFoundError:
        print('Could not find GPS csv file')
        exit(1)
    except pd.errors.EmptyDataError:
        print('Empty File')
        exit(1)
    except pd.errors.ParserError:
        print('Could not parse file')
        exit(1)
    
    return data


def get_markers(file_path):
    """
    Returns a pandas dataframe from the file path for marker gps data
    :param file_path : The file path to the gps file
    :type file_path : string
    :return : The pandas dataframe
    :rtype : DataFram
    """

    # read into a pandas dataframe
    try:
        data = pd.read_csv(file_path, names=['LONTITUDE', 'LATITUDE'], sep=',')
    except FileNotFoundError:
        print('Could not find GPS csv file')
        exit(1)
    except pd.errors.EmptyDataError:
        print('Empty File')
        exit(1)
    except pd.errors.ParserError:
        print('Could not parse file')
        exit(1)
    
    return data


def read_data():
    """
    Reads the GPS data file and returns latitudes, longitudes and average speed
    :return : Lotitudes, Longitudes and average speed
    :rtype : list, list, int
    """

    file_path = 'data/data.csv'
    
    # read into a pandas dataframe
    data = get_data(file_path)
    
    # transform to seconds
    data['TIME'] = data['TIME'].apply(make_seconds)

    # make sure the time series makes sense by sorting
    data = data.sort_values('TIME')

    # total time travelled in seconds

    time_delta_s = (data['TIME'][len(data)-1] - data['TIME'][0])

    total_distance = 0

    # get the geodesic distance travelled in km
    for i in range(len(data)):
        # nothing to do for just one point
        if i != 0:
            point1 = (data['LATITUDE'][i], data['LONTITUDE'][i])
            point2 = (data['LATITUDE'][i-1], data['LONTITUDE'][i-1])
            segment_distance = distance.distance(point1, point2).km
            total_distance = total_distance + segment_distance

    print(f'Travelled {total_distance} km in {time_delta_s/60} minutes')

    # make lists
    latitude_list = data['LATITUDE'].tolist()
    longitude_list = data['LONTITUDE'].tolist()

    # calculate average speed in km/h
    average_speed_kmh = int(total_distance/(time_delta_s/3600))
    print(f'average speed is: {average_speed_kmh}kmph\n')

    return latitude_list, longitude_list, average_speed_kmh

def main():
    """
    Read and parse GPS data
    :return : None
    """
    read_data()

if __name__ == "__main__":
    main()
