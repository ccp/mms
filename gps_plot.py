"""
file: gps_plot.py
brief: Flask app for GPS trace plot
author: Caswell Pieters
date: 27 February 2022
"""

from flask import Flask,render_template
from sqlalchemy import true
# import get_gps

from gps_class import GPS

# Freemium API key 
api_key = 'gtiAtbsKwwxZxiLxpPrxdkqHMxeOc274RwWwdSLrbeY' 

# User marker segments enabled or not
use_markers = 0

gps = GPS(api_key=api_key)

raw_data = gps.get_data(file_path='data/data.csv')

raw_marker_data = gps.get_markers(file_path='data/markers.csv')

gps_data = gps.read_data(data=raw_data)

# are we using marker segments
if use_markers == 1:
    # run only if we're using marker segments
    gps.read_markers((raw_marker_data))
    gps.get_segment_speeds(gps_data=gps_data, marker_data=gps.marker_data)
    gps.create_chart(segment_speeds=gps.segment_speeds, average_speed=gps_data[2])

# Flask code 
app = Flask(__name__)
@app.route('/')

def gps_map():
    # html in map.html
	return render_template('map.html',apikey=api_key, gps_data=gps_data,
                            marker_data=gps.marker_data, segment_speeds=gps.segment_speeds, use_markers=use_markers)

if __name__ == '__main__':
    app.run(debug = False)