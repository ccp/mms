"""
file: gps.py
brief: GPS data class
date: 03 March 2022
"""

from datetime import datetime

from geopy import distance
import matplotlib.pyplot as plt
import pandas as pd


class GPS:
    """
    The base GPS class
    Attributes
    ----------
    api_key : string
        API key
    total_distance : float
        total distance travelled in km
    time_delta_s: int
        totaltime of travel
    lats : list
        list of all latitudes
    longs : longs
        list of all longitudes
    timestamps: int
        list of all timestamps

    Methods
    -------
    make_seconds(time_element)
        Turn the string date time into seconds integer
    get_data(file_path)
        Takes the file path and returns pandas DataFrame
    read_data(data)
        Returns coordinates and average speed in kmph from dataframe
    get_markers(file_path)
        Returns lists of the marker coordinates
    get_segment_speeds(gps_data, marker_data)
        Takes the GPS data coordinate lists and marker data lists and returns speeds between markers
    create_cart(segment_speeds)
        Takes the segment speeds and saves it to a chart
    """

    def __init__(self, api_key):
        """
        Creates appropriate spectra and data objects based on the technique.
        :param api_key : Your API key
        :type api_key : string
        :param file_path : Path to GPS data file
        :type file_path : Path
        :return :
        """

        self.api_key = api_key

        self.total_distance = 0

        self.time_delta_s = 0

        self.lats = []

        self.longs = []

        self.timestamps = []
        
        self.marker_data = []

        self.segment_speeds = []


    def make_seconds(self,time_element):
        """
        Returns the date in seconds
        :param time_element : The date/time from the GPS data
        :type time_element : String
        :return : Date time in seconds
        :rtype :int
        """

        date = datetime.strptime(time_element, '%Y-%m-%d %H:%M:%S')
        time_stamp = date.timestamp()
        return time_stamp


    def get_data(self, file_path):
        """
        Returns a pandas dataframe from the file path to gps data
        :param file_path : The file path to the gps file
        :type file_path : string
        :return : The pandas dataframe
        :rtype : DataFram
        """

        # read into a pandas dataframe
        try:
            data = pd.read_csv(file_path, names=['ID', 'TIME', 'LONTITUDE', 'LATITUDE'], sep=',')
        except FileNotFoundError:
            print('Could not find GPS csv file')
            exit(1)
        except pd.errors.EmptyDataError:
            print('Empty File')
            exit(1)
        except pd.errors.ParserError:
            print('Could not parse file')
            exit(1)
        
        return data


    def get_markers(self, file_path):
        """
        Returns a pandas dataframe from the file path to gps markers
        :param file_path : The file path to the gps file
        :type file_path : string
        :return : The pandas dataframe
        :rtype : DataFram
        """

        # read into a pandas dataframe
        try:
            data = pd.read_csv(file_path, names=['LONTITUDE', 'LATITUDE'], sep=',')
        except FileNotFoundError:
            print('Could not find GPS csv file')
            exit(1)
        except pd.errors.EmptyDataError:
            print('Empty File')
            exit(1)
        except pd.errors.ParserError:
            print('Could not parse file')
            exit(1)
        
        return data


    def read_data(self, data):
        """
        Reads the GPS data file and returns latitudes, longitudes and average speed
        :return : Lotitudes, Longitudes  average speed, and timestamps
        :rtype : tuple
        """
        # transform to seconds
        data['TIME'] = data['TIME'].apply(self.make_seconds)

        # make sure the time series makes sense by sorting
        data = data.sort_values('TIME')

        # total time travelled in seconds

        self.time_delta_s = (data['TIME'][len(data)-1] - data['TIME'][0])

        self.total_distance = 0

        # get the geodesic distance travelled in km
        for i in range(len(data)):
            # nothing to do for just one point
            if i != 0:
                point1 = (data['LATITUDE'][i], data['LONTITUDE'][i])
                point2 = (data['LATITUDE'][i-1], data['LONTITUDE'][i-1])
                segment_distance = distance.distance(point1, point2).km
                self.total_distance = self.total_distance + segment_distance

        print(f'Travelled {self.total_distance} km in {self.time_delta_s/60} minutes')

        # make lists
        self.lats = data['LATITUDE'].tolist()
        self.longs = data['LONTITUDE'].tolist()
        self.timestamps = data['TIME'].tolist()

        # calculate average speed in km/h
        average_speed_kmh = int(self.total_distance/(self.time_delta_s/3600))
        print(f'average speed is: {average_speed_kmh}kmph\n')

        return self.lats, self.longs, average_speed_kmh, self.timestamps


    def get_segment_speeds(self, gps_data, marker_data):
        """
        Takes the GPS data coordinate lists and marker data lists and returns speeds between markers
        :param gps_data: The coordinates and timestamps of the gps data 
        :param gps_data: tuple
        :param marker_data: The coordinates of the marker data 
        :param marker_data: tuple
        :return : average segment speeds
        :rtype : list
        """

        # create a coordinate tuple list for the gps data
        coords = list(zip(gps_data[0], gps_data[1]))
        # create a coordinate tuple list for the marker data
        marker_coords = list(zip(marker_data[0], marker_data[1]))
        speeds_list = []
        # look for the marker data in the gps data
        last_index = 0
        for index, coordinate in enumerate(coords):
            if coordinate in marker_coords:
                segment_distance = distance.distance(coordinate, coords[last_index]).km
                segment_time_s = gps_data[3][index] - gps_data[3][last_index]
                if segment_time_s != 0:
                    segment_speed = segment_distance/(segment_time_s/3600)
                else:
                    segment_speed = 0
                speeds_list.append(segment_speed)

        self.segment_speeds = speeds_list
        return speeds_list


    def read_markers(self, data):
        """
        Reads the GPS data marker file and returns latitudes, longitudes 
        :return : Lotitudes, Longitudes 
        :rtype : tuple
        """

        # make lists
        marker_lats = data['LATITUDE'].tolist()
        marker_longs = data['LONTITUDE'].tolist()

        self.marker_data = (marker_lats, marker_longs)

        return marker_lats, marker_longs


    def create_chart(self, segment_speeds, average_speed):
        """
        Takes the segment speeds and saves it to a chart
        :param segment_speeds : The average speeds of all the segments between markers
        :type segment_speeds : list
        :param average_speed : Average speed for the whole trace
        :type average_speed : float
        :return : None
        """

        y = segment_speeds
        x = []
        for i in range(len(segment_speeds)):
            label = "Seg %d" % i
            x.append(label)
        plt.plot(x, y)
        plt.title('Average speed per marker segment')
        plt.xlabel('Marker Segments')
        plt.ylabel('Average speed km/h')
        average_speed_text = 'Average speed: %d km/h' % average_speed
        plt.text(0, 0, average_speed_text, bbox=dict(facecolor='red', alpha=0.3))
        plt.savefig('static/images/chart.png')