"""
file: test_gps_class.py
brief: Unit test for GPS class
author: Caswell Pieters
date: 3 March 2022
"""

from gps_class import GPS

import pandas as pd

def test_make_seconds():
    """Test for turning date text into seconds"""

    gps = GPS(api_key='gtiAtbsKwwxZxiLxpPrxdkqHMxeOc274RwWwdSLrbeY')


    date = "2008-02-02 16:36:08"

    assert gps.make_seconds(date) == 1201962968.0; 'test failed'


def test_get_data():
    """Test getting pandas datafram from gps file"""

    gps = GPS(api_key='gtiAtbsKwwxZxiLxpPrxdkqHMxeOc274RwWwdSLrbeY')

    raw_data = gps.get_data(file_path='data/data.csv')

    is_dataframe = isinstance(raw_data, pd.DataFrame)

    assert is_dataframe is True; 'test failed'


def test_get_markers():
    """Test getting pandas datafram from gps markers file"""

    gps = GPS(api_key='gtiAtbsKwwxZxiLxpPrxdkqHMxeOc274RwWwdSLrbeY')

    raw_data = gps.get_markers(file_path='data/markers.csv')

    is_dataframe = isinstance(raw_data, pd.DataFrame)

    assert is_dataframe is True; 'test failed'


def test_get_segments_speeds():
    """Test getting the average speed between marker segments"""

    gps = GPS(api_key='gtiAtbsKwwxZxiLxpPrxdkqHMxeOc274RwWwdSLrbeY')

    gps_data = ([39.9158, 39.85165],[116.52231, 116.69172 ], 3 , [1201912257 ,1201912857])
    marker_data = ([39.85165], [116.69172])

    assert gps.get_segment_speeds(gps_data=gps_data, marker_data=marker_data)[0] == 96.88180344727854; 'test failed'